extends Area2D

# class member variables go here, for example:
# var a = 2
# var b = "textvar"
var screenSize
export var speed = 300

onready var bullet = preload("res://Scenes/Laser.tscn")

func _ready():
	# Called when the node is added to the scene for the first time.
	# Initialization here
	screenSize = get_viewport_rect().size

func _process(delta):
	# Called every frame. Delta is time since last frame.
	# Update game logic here.
	var velocity = 0
	if Input.is_action_pressed("ui_right"):
		velocity = 1
	if Input.is_action_pressed("ui_left"):
		velocity = -1
	
	position.x += velocity * speed * delta
	position.x = clamp(position.x, 0, screenSize.x)
	
	if Input.is_action_just_pressed("ui_select"):
		var n_bullet = bullet.instance()
		n_bullet.position = position
		n_bullet.position.y -= 30
		get_node("/root/Main").add_child(n_bullet)
